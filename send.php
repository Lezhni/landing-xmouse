<?php

if ($_POST) {
    require 'phpmailer/PHPMailerAutoload.php';

    $mail = new PHPMailer;

    $mail->CharSet = 'UTF-8';
    $mail->setFrom('welcome@xmouse.ru', 'XMouse');
    $mail->addReplyTo('welcome@xmouse.ru', 'XMouse');
    $mail->addAddress('welcome@xmouse.ru');
    $mail->addAddress('direct@xmouse.ru');
    $mail->isHTML(true);

    switch ($_POST['formtype']) {
        case 'callback':
            $mail->Subject = 'Заявка на обратный звонок';
            break;
        case 'presentation':
            $mail->Subject = 'Заявка на получение презентации';
            break;
        default:
            $mail->Subject = 'Заявка на обратный звонок';
    }

    $mail->Body = "<b>Имя:</b> {$_POST['contact']}<br><br><b>Номер телефона:</b> {$_POST['phone']}";
    if (!empty($_POST['email']))
        $mail->Body .= "<br><br><b>Email:</b> {$_POST['email']}";
    if (!empty($_POST['company']))
        $mail->Body .= "<br><br><b>Название компании:</b> {$_POST['company']}";
    if (!empty($_POST['companypost']))
        $mail->Body .= "<br><br><b>Должность:</b> {$_POST['companypost']}";

    if($mail->send()) {
        echo 'sended';
    } else {
        echo $mail->ErrorInfo;
    }
}

die();
