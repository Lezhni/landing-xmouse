$ = jQuery;

// #menu scrollspy
var lastId;
var topMenu = $(".header-menu");
var topMenuHeight = 47;
var menuItems = topMenu.find("a");

var mousewheelPos = 0;

$(document).on('ready', init);
$(window).on('load', loaded);
$(window).on('scroll', scrolling);
//
$(document).on('click', '.header-menu a:not(.active)', scrollToSection);
$(document).on('click', '.first-screen-scroll', hideFirstScreen);
$(document).on('click', '.popup-close', closePopup);
$(document).on('submit', 'form', submitForm);

$('.promo-about-single').hover(function() {

    if (!$(this).hasClass('active')) {
        var targetText = $('.promo-text p[data-for=' + $(this).attr('id') + ']');
        $('.promo-about-single').removeClass('active');
        $(this).addClass('active');
        $('.promo-text p').removeAttr('style').removeClass('active');
        targetText.show().animate({
            left: 0,
            opacity: 1,
        }, 300).addClass('active');
    }
});

// #first screen scroll
var tr = 1;
var op = 1;
if ($(window).width() <= 1024) {
    var scaleLim = 11;
} else {
    var scaleLim = 14;
}

$(document).on('mousewheel', function(e) {
    var delta = e.originalEvent.wheelDelta;
    tr = tr + (-delta/150);
    op = op + (delta/1300);
    if (tr <= 1)
        tr = 1;
    if (op >= 1)
        op = 1;

    if ($('body').hasClass('first-screen-displayed')) {

        if (tr >= scaleLim) {
            $('body').removeClass('first-screen-displayed')
            $('.xmouse-first-screen').hide();
        } else {
            $('.xmouse-first-screen').css({
                transform: 'scale(' + tr + ')',
                opacity: op
            });
        }
    }
});

$('.xmouse-first-screen').on('click', function(event) {
    if (!$(event.target).hasClass('popup-callback'))
        hideFirstScreen();
});

$(window).resize(function() {
    decorScroll();
});

$(document).on('click', '.popup-callback', function() {

    $('body').addClass('remove-overflow');
    $('.fullscreen-popup-bg').animate({
        transform: 'scale(10)'
    }, 500);

    if ($(window).width() <= 1040) {
        var formDelay = 100;
    } else {
        var formDelay = 300;
    }
    $('.fullscreen-popup.callback').delay(formDelay).fadeIn(300);

    return false;
});

$(document).on('click', '.gallery-button', function() {

    $.magnificPopup.open({
        items: {
            src: '#presentation',
            type: 'inline'
        }
    });

    return false;
});

function init() {
    if ($(window).scrollTop() <= 10) {
        $('body').addClass('first-screen-displayed');
        $('.xmouse-first-screen').show();
    }

    // #menu scrollspy
    scrollItems = menuItems.map(function(){
      var item = $($(this).attr("href"));
      if (item.length) { return item; }
    });

    $('input[name=phone]').inputmask({
  	    mask: '+7 (999) 999-99-99',
  	    showMaskOnHover: false,
  	    showMaskOnFocus: true,
  	});
}

$('.xmouse-figures').on('scrollSpy:enter', function() {

    var speed = 450;

    if (!$(this).hasClass('visible')) {
        $(this).addClass('visible');
        $(".girl-body-point").stop().animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".girl-body-line").stop().animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".girl-body-text").stop().animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');

        $(".man-body-point").stop().delay(speed*2).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".man-body-line").stop().delay(speed*2).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".man-body-text").stop().delay(speed*2).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');

        $(".girl-watch-point").stop().delay(speed*3).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".girl-watch-line").stop().delay(speed*3).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".girl-watch-text").stop().delay(speed*3).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');

        $(".man-phone-point").stop().delay(speed*4).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".man-phone-line").stop().delay(speed*4).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".man-phone-text").stop().delay(speed*4).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');

        $(".girl-phone-point").stop().delay(speed*5).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".girl-phone-line").stop().delay(speed*5).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".girl-phone-text").stop().delay(speed*5).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');

        $(".man-watch-point").stop().delay(speed*6).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".man-watch-line").stop().delay(speed*6).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".man-watch-text").stop().delay(speed*6).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');

        $(".girl-bag-point").stop().delay(speed*7).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".girl-bag-line").stop().delay(speed*7).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".girl-bag-text").stop().delay(speed*7).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');

        $(".man-bag-point").stop().delay(speed*8).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".man-bag-line").stop().delay(speed*8).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".man-bag-text").stop().delay(speed*8).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');

        $(".girl-legs-point").stop().delay(speed*9).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".girl-legs-line").stop().delay(speed*9).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".girl-legs-text").stop().delay(speed*9).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');

        $(".man-legs-point").stop().delay(speed*10).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".man-legs-line").stop().delay(speed*10).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
        $(".man-legs-text").stop().delay(speed*10).animate({'opacity': 1, transform: 'none'}, speed).addClass('visible');
    }
});

$('.xmouse-figures').scrollSpy();

function loaded() {

    $('.preloader').fadeOut(400, function() {
        $('body').removeClass('remove-overflow');
        decorScroll();
    });
}

function scrolling() {

    var fromTop = $(this).scrollTop() + topMenuHeight;

    var cur = scrollItems.map(function(){
     if ($(this).offset().top < fromTop)
       return this;
    });

    cur = cur[cur.length-1];
    var id = cur && cur.length ? cur[0].id : "";

    if (lastId !== id) {
        lastId = id;
        menuItems.removeClass("active");
        $('.header-menu a[href="#' + id + '"]').addClass('active');
        decorScroll();
    }
}

function decorScroll() {

    if ($('.header-menu a.active').length) {
        var left = $('.header-menu a.active').position().left + ($('.header-menu a.active').innerWidth()/2 - 3.5);
        $('.header-menu-scroller').css({
            left: left,
            opacity: 1
        });
    }
}

function scrollToSection() {

    var href = $(this).attr("href");
    var offsetTop = href === "#" ? 0 : $(href).offset().top - topMenuHeight + 1;

    $('html, body').stop().animate({
        scrollTop: offsetTop
    }, 500);
    //decorScroll();

    return false;
}

function submitForm() {

    var form = $(this);
    var formData = form.serialize();

    form.find('input[type=submit]').attr('disabled', 'disabled').val('Отправляем...');

    if (formData) {
        $.post('send.php', formData, function(data) {
            if (data == 'sended') {
                $.magnificPopup.close();
                form.find('input[type="text"], input[type="email"], input[type="tel"]').val('');
                $('body').addClass('remove-overflow');
                $('.fullscreen-popup-bg').animate({
                    transform: 'scale(10)'
                }, 550);

                if ($(window).width() <= 1040) {
                    var formDelay = 100;
                } else {
                    var formDelay = 300;
                }
                $('.fullscreen-popup.callback').fadeOut(300);
                $('.fullscreen-popup.thanks').delay(formDelay).fadeIn(300);
            } else {
                console.log(data);
            }
        });
    }

    form.find('input[type=submit]').removeAttr('disabled').val('Перезвоните мне');

    return false;
}

function hideFirstScreen() {

    $('body').removeClass('first-screen-displayed');

    $('.xmouse-first-screen').animate({
        transform: 'scale(15)',
        opacity: 0
    }, 600, 'linear', function() {
        $('.xmouse-first-screen').hide();
        decorScroll();
    });
}

function closePopup() {

    $('.fullscreen-popup').fadeOut(300);
    $('.fullscreen-popup-bg').animate({
        transform: 'scale(0)'
    }, 500, function() {
        $('body').removeClass('remove-overflow');
    });
}
